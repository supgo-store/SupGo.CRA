﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupGo.DAL.NetCore.Services;
using SupGo.DAL.NetCore.Models;

public static class CustomizedRecommandationAlgorithm
{
    /// <summary>
    /// Get the recommendations for a given User
    /// </summary>
    /// <param name="uuid"></param>
    /// <returns>Sorted list of recommended products</returns>
    public static List<Product> GetRecommendations(string uuid)
    {
        // Getting the user
        User user = DatabaseService<User>.GetSingle(true, u => u.Uuid == uuid);

        // Getting all products in customer's cart
        List<string> products_in_cart = GetCurrentUserCart(uuid);
        List<List<string>> all_commands = GetAllCommands();
        List<string> filtered_products_of_commands = GetFilteredProductsOfCommands(all_commands, products_in_cart);

        var unsorted_recommendations = ComputeCRA(filtered_products_of_commands, user);

        // Removing products in cart from the dictionnary
        foreach (string products_in_cart_name in products_in_cart)
        {
            unsorted_recommendations.Remove(products_in_cart_name);
        }

        List<Product> recommended_products = SortRecommendations(unsorted_recommendations);

        return recommended_products;
    }

    /// <summary>
    /// Get current user cart
    /// </summary>
    /// <param name="uuid"></param>
    /// <returns>List of product names</returns>
    public static List<string> GetCurrentUserCart(string uuid)
    {
        Cart user_cart = DatabaseService<Cart>.GetSingle(true, c => c.UserUuid == uuid && c.Datetime == null);
        List<string> product_in_cart = new List<string>();
        if (user_cart != null)
        {
            IList<Orders> orders = DatabaseService<Orders>.GetList(true, order => order.CartUuid == user_cart.Uuid);
            foreach (Orders o in orders)
            {
                Product product = DatabaseService<Product>.GetSingle(true, p => p.Uuid == o.ProductUuid);
                product_in_cart.Add(product.Name);
            }
        }

        return product_in_cart;
    }

    /// <summary>
    /// Get all commands just with product names 
    /// </summary>
    /// <returns>List of string with all commands</returns>
    public static List<List<string>> GetAllCommands()
    {
        List<List<string>> all_commands = new List<List<string>>();

        List<Cart> carts = DatabaseService<Cart>.GetAll(true).ToList();
        foreach (Cart cart in carts)
        {
            IList<Orders> orders = DatabaseService<Orders>.GetList(true, order => order.CartUuid == cart.Uuid);
            List<string> command = new List<string>();
            foreach (Orders order in orders)
            {
                command.Add(DatabaseService<Product>.GetSingle(true, p => p.Uuid == order.ProductUuid).Name);
            }
            all_commands.Add(command);
        }

        return all_commands;
    }

    /// <summary>
    /// Get all the products of the commands which contains at least one of the products in cart
    /// </summary>
    /// <param name="all_commands"></param>
    /// <param name="products_in_cart"></param>
    /// <returns>List of filtered products</returns>
    public static List<string> GetFilteredProductsOfCommands(List<List<string>> all_commands, List<string> products_in_cart)
    {
        List<List<string>> filtered_commands = new List<List<string>>();
        List<string> filtered_products_of_commands = new List<string>();

        foreach (List<string> command in all_commands)
        {
            foreach (string products_in_cart_name in products_in_cart)
            {
                // Creating list by checking if the command contains one of the products in cart
                if (command.Contains(products_in_cart_name))
                {
                    foreach (string product_name in command)
                    {
                        filtered_products_of_commands.Add(product_name);
                    }
                }
            }
        }

        return filtered_products_of_commands;
    }

    /// <summary>
    /// Compute the Customized Recommendation Algorithm 
    /// </summary>
    /// <param name="values"></param>
    /// <param name="user"></param>
    /// <returns>Unsorted list of recommended products</returns>
    public static Dictionary<string, int> ComputeCRA(List<string> values, User user)
    {
        var result = new Dictionary<string, int>();
        List<List<string>> user_commands = new List<List<string>>();
        List<string> all_user_items = new List<string>();

        IList<Cart> carts = DatabaseService<Cart>.GetList(true, c => c.UserUuid == user.Uuid);

        // Getting all the user's items and commands
        foreach (Cart cart in carts)
        {
            IList<Orders> orders = DatabaseService<Orders>.GetList(true, order => order.CartUuid == cart.Uuid);
            List<string> command = new List<string>();
            foreach (Orders order in orders)
            {
                string product_name = DatabaseService<Product>.GetSingle(true, p => p.Uuid == order.ProductUuid).Name;
                command.Add(product_name);
                all_user_items.Add(product_name);
            }
            user_commands.Add(command);
        }

        List<string> discounts = GetAllNameOfDiscountedProducts();

        var counts = all_user_items.GroupBy(x => x)
           .Select(g => new { Item = g.Key, Count = g.Count() })
           .ToList();

        foreach (string value in values)
        {
            if (result.TryGetValue(value, out int count))
            {
                // Increase existing value
                result[value] = count + 1;
            }
            else
            {
                // Setting the new value to 1
                result.Add(value, 1);
            }
        }

        foreach (string product in result.Keys.ToList())
        {
            int tmp = result[product];
            //Testing if the user has already taken the product (Only if he have more than 5 commands)
            if (all_user_items.Contains(product) && user_commands.Count() > 5)
            {
                var product_name = counts.Find(item => item.Item == product);
                result[product] = tmp + product_name.Count * 3;
            }
            //Testing if the product is in promotion
            if (discounts.Contains(product))
            {
                result[product] = result[product] + tmp * 2;
            }
        }

        return result;
    }

    /// <summary>
    /// Get all the names of discounted products
    /// </summary>
    /// <returns>List of discounted product names </returns>
    public static List<string> GetAllNameOfDiscountedProducts()
    {
        List<string> discs = new List<string>();
        List<Discount> discounts = DatabaseService<Discount>.GetAll(true).ToList();
        foreach (Discount d in discounts)
        {
            Product p = DatabaseService<Product>.GetSingle(true, product => product.Uuid == d.ProductUuid);
            discs.Add(p.Name);
        }

        return discs;
    }

    /// <summary>
    /// Display properly a list of recommendations
    /// </summary>
    /// <param name="recommandations"></param>
    /// <returns>Sorted list of recommended products</returns>
    public static List<Product> SortRecommendations(Dictionary<string, int> unsorted_recommendations)
    {
        // Order pairs in dictionary from high to low frequency
        var sorted = from pair in unsorted_recommendations
                     orderby pair.Value descending
                     select pair;

        int sorted_lenght = sorted.Count();

        List<Product> recommendations = new List<Product>();
        for (var i = 0; i < sorted_lenght; i++)
        {
            recommendations.Add(DatabaseService<Product>.GetSingle(true, p => p.Name == sorted.ElementAt(i).Key));
        }

        return recommendations;
    }
}